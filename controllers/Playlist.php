<?php
function __autoload($className){
	include_once("../models/$className.php");	
}

$playlists = new Playlist("localhost","root","portmore254","b_tunes");

if(!isset($_POST['action'])) {
	print json_encode(0);
	return;
}

switch($_POST['action']) {
	case 'getPlaylists':
		print $playlists->getPlaylists();		
	break;
	
	case 'addPlaylist':
		$playlist = new stdClass;
		$playlist = json_decode($_POST['playlist']);
		print $playlists->addPlaylist($playlist);		
	break;
	
	case 'deletePlaylist':
		$playlist = new stdClass;
		$playlist = json_decode($_POST['playlist']);
		print $playlists->deletePlaylist($playlist);		
	break;
	
	case 'updatePlaylist':
		$playlist = new stdClass;
		$playlist = json_decode($_POST['playlist']);
		print $playlists->updatePlaylist($playlist);				
	break;
}

exit();