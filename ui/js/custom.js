$(function() {
	$(document).on("click", "a#playlist_list", function(){ getPlaylistList(this); });	
	$(document).on("click", "a#create_playlist_form", function(){ getCreateForm(this); });	
	$(document).on("click", "button#add_playlist", function(){ addPlaylist(this); });
	$(document).on("click", "a.delete_confirm", function(){ deleteConfirmation(this); });
	$(document).on("click", "button.delete", function(){ deletePlaylist(this); });
	$(document).on("dblclick", "td.edit", function(){ makeEditable(this); });
	$(document).on("blur", "input#editbox", function(){ removeEditable(this) });
});

function removeEditable(element) { 
	
	$('#indicator').show();
	
	var Playlist = new Object();
	Playlist.playlist_id = $('.current').attr('playlist_id');		
	Playlist.field = $('.current').attr('field');
	Playlist.newvalue = $(element).val();
	
	var playlistJson = JSON.stringify(Playlist);
	
	$.post('controllers/Playlist.php',
		{
			action: 'updatePlaylist',			
			playlist: playlistJson
		},
		function(data, textStatus) {
			$('td.current').html($(element).val());
			$('.current').removeClass('current');
			$('#indicator').hide();			
		}, 
		"json"		
	);	
}

function makeEditable(element) { 
	$(element).html('<input id="editbox" size="'+  $(element).text().length +'" type="text" value="'+ $(element).text() +'">');  
	$('#editbox').focus();
	$(element).addClass('current'); 
}

function deleteConfirmation(element) {	
	$("#delete_confirm_modal").modal("show");
	$("#delete_confirm_modal input#playlist_id").val($(element).attr('playlist_id'));
}

function deletePlaylist(element) {	
	
	var Playlist = new Object();
	Playlist.playlist_id = $("#delete_confirm_modal input#playlist_id").val();
	
	var playlistJson = JSON.stringify(Playlist);
	
	$.post('controllers/Playlist.php',
		{
			action: 'deletePlaylist',
			playlist: playlistJson
		},
		function(data, textStatus) {
			getPlaylistList(element);
			$("#delete_confirm_modal").modal("hide");
		}, 
		"json"		
	);	
}

function getPlaylistList(element) {
	
	$('#indicator').show();
	
	$.post('controllers/Playlist.php',
		{
			action: 'getPlaylists'				
		},
		function(data, textStatus) {
			renderPlaylistList(data);
			$('#indicator').hide();
		}, 
		"json"		
	);
}

function renderPlaylistList(jsonData) {
	
	var table = '<table width="600" cellpadding="5" class="table table-hover table-bordered"><thead><tr><th scope="col">Playlist Name</th><th scope="col">Description</th><th scope="col">Created</th><th scope="col">Modified</th><th scope="col">Delete</th></tr></thead><tbody>';

	$.each( jsonData, function( index, playlist){     
		table += '<tr>';
		table += '<td class="edit" field="playlist_name" playlist_id="'+playlist.playlist_id+'">'+playlist.playlist_name+'</td>';
		table += '<td class="edit" field="description" playlist_id="'+playlist.playlist_id+'">'+playlist.description+'</td>';
		table += '<td class="edit" field="created" playlist_id="'+playlist.playlist_id+'">'+playlist.created+'</td>';
		table += '<td class="edit" field="modified" playlist_id="'+playlist.playlist_id+'">'+playlist.modified+'</td>';
		table += '<td><a href="javascript:void(0);" playlist_id="'+playlist.playlist_id+'" class="delete_confirm btn btn-danger"><i class="icon-remove icon-white"></i></a></td>';
		table += '</tr>';
    });
	
	table += '</tbody></table>';
	
	$('div#content').html(table);
}

function addPlaylist(element) {	
	
	$('#indicator').show();
	
	var Playlist = new Object();
	Playlist.playlist_name = $('input#playlist_name').val();
	Playlist.description = $('input#description').val();
	
	var playlistJson = JSON.stringify(Playlist);
	
	$.post('controllers/Playlist.php',
		{
			action: 'addPlaylist',
			playlist: playlistJson
		},
		function(data, textStatus) {
			getPlaylistList(element);
			$('#indicator').hide();
		},
		"json"		
	);
}

function getCreateForm(element) {
	var form = '<div class="input-prepend">';
		form +=	'<span class="add-on"><i class="icon-headphones icon-black"></i>Name</span>';
		form +=	'<input type="text" id="playlist_name" name="playlist_name" value="" class="input-xlarge" />';		
		form +=	'</div><br/><br/>';

		form +=	'<div class="input-prepend">';
		form +=	'<span class="add-on"><i class="icon-book icon-black"></i> Description</span>';
		form +=	'<input type="text" id="description" name="description" value="" class="input-xlarge" />';
		form +=	'</div><br/><br/>';

		form +=	'<div class="control-group">';
		form +=	'<div class="">';		
		form +=	'<button type="button" id="add_playlist" class="btn btn-primary"><i class="icon-ok icon-white"></i> Add Playlist</button>';
		form +=	'</div>';
		form +=	'</div>';
		
		$('div#content').html(form);
}