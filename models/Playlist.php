<?php

class Playlist {
	
	private $dbConnect;
	
	public function __construct($host,$user,$password,$db)	{		
		$this->dbConnect = new PDO("mysql:host=".$host.";dbname=".$db,$user,$password);		
	}

	public function getPlaylists(){				
		$get = $this->dbConnect->prepare("SELECT * FROM playlist");
		$get->execute();
		return json_encode($get->fetchAll());
	}

	public function addPlaylist($playlist){		
		$add = $this->dbConnect->prepare("INSERT INTO playlist(playlist_name, description) VALUES (?,?)");
		$add->execute(array($playlist->playlist_name, $playlist->description));		
		return json_encode($this->dbConnect->lastInsertId());
	}
	
	public function deletePlaylist($playlist){				
		$delete = $this->dbConnect->prepare("DELETE FROM playlist WHERE playlist_id=?");
		$delete->execute(array($playlist->playlist_id));
		return json_encode($delete);
	}
	
	public function updatePlaylist($playlist){		
		$update = $this->dbConnect->prepare("UPDATE playlist SET ". $playlist->field ."=? WHERE playlist_id=?");
		$update->execute(array($playlist->newvalue, $playlist->playlist_id));				
		return json_encode(1);	
	}
}
?>